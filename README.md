# Crontab Task

## Introduction

This coding task is all about seeing how well you can take a spec and produce a functional
solution.

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone https://kkbokyere@bitbucket.org/kkbokyere/cronjob.git
```

**2. Install Dependencies.**

Once that's all done, cd into the crontab directory and install the depedencies:

```
$ cd crontab
$ npm install
```

**3. Run Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. In your terminal run:

```
$ node index.js 16:10
```

## Improvements
- I dont really understand the logic behind the * , so ideally I would have implemented the logic on how to display the day the command would run
