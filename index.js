//Input
//30 1 /bin/run_me_daily
// 45 * /bin/run_me_hourly
// * * /bin/run_me_every_minute
// * 19 /bin/run_me_sixty_times

// Output
// 1:30 tomorrow - /bin/run_me_daily
// 16:45 today - /bin/run_me_hourly
// 16:10 today - /bin/run_me_every_minute
// 19:00 today - /bin/run_me_sixty_times

const readline = require("readline");
const fs = require("fs");

function addMinutes(date, minutes, hourOfDay) {
    const o = new Intl.DateTimeFormat("en-GB" , {
        timeStyle: "short",
        hour12: false
    });
    if (hourOfDay !== '*') {
        date.setHours(hourOfDay)
    }
    if (minutes !== '*') {
        date.setMinutes(minutes)
    }
    return o.format(date)
}

function addDayToRun(date, hourOfDay) {
    if (hourOfDay === '*') {
        return "today"
    }
    return "tomorrow"
}

function createJobOutput(currentTime, config) {
    const convertedDateTime = new Date(
        `${new Date().toDateString()},${currentTime}`
    );

    const splitString = config.split(" ");
    const minutes = splitString[0];
    const hourOfDay = splitString[1];
    const command = splitString[2];

    const minutesOutput = addMinutes(convertedDateTime, minutes, hourOfDay);

    const hourOutput = addDayToRun(convertedDateTime, hourOfDay);
    return `${minutesOutput} ${hourOutput} - ${command}`;
}

const lineReader = readline.createInterface({
    input: fs.createReadStream('./config.txt')
});

lineReader.on('line', function (line) {
    const currentTimeArg = process.argv.slice(2);
    console.log(createJobOutput(currentTimeArg[0], line))
});
